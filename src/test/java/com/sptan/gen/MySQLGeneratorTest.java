package com.sptan.gen;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * MySQL 代码生成
 *
 * @author lp
 * @since 3.5.3
 */
public class MySQLGeneratorTest extends BaseGeneratorTest {

    private static final String url = "jdbc:mysql://localhost:3306/ssmp?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai";
    private static final String username = "root";

    private static final String password = "1234qweR";


    @Test
    public void testSimple() {
        FastAutoGenerator generator = FastAutoGenerator.create(url, username, password);
        generator.strategyConfig(builder -> {
            builder
                .addTablePrefix("sys_")
                .addInclude("sys_menu")
                .entityBuilder()
                .enableLombok()
                // 这些是共通字段
                .addSuperEntityColumns("id", "delete_flag", "version", "ctime", "utime", "cuid", "opuid")
                // entity的基类
                .superClass("com.sptan.framework.mybatis.entity.AbstractFocusBaseEntity")
                .serviceBuilder()
                .formatServiceFileName("%sService")
                .controllerBuilder()
                .enableRestStyle()
                .build();
        });
        generator.globalConfig(builder -> {
            builder
                .author("lp")
                .enableSpringdoc()
                .commentDate("yyyy-MM-dd")
                // 代码生成路径，是我们上面讨论的工程的代码
                .outputDir("/Users/liupeng/dev/springboot3-springsecurity6-mybatisplus/src/main/java")
                .build();
        });

        generator.templateConfig(builder -> {
            // service和Controller使用了自定义的模板
            builder
                .service("templates/service.java")
                .serviceImpl("templates/serviceImpl.java")
                .controller("templates/controller.java")
                .build();
        });

        generator.packageConfig(builder -> {
            // entity的模板定制内容，改变了默认的xml文件放置位置
            builder
                .parent("com.sptan.ssmp")
                .entity("domain")
                .pathInfo(Collections.singletonMap(OutputFile.xml,
                    "/Users/liupeng/dev/springboot3-springsecurity6-mybatisplus/src/main/resources/mapper"))
                .build();
        });
        generator.injectionConfig(builder -> {
            Map<String, String> customFile = new HashMap<>();
            // 新增了DTO、VO的生成
            customFile.put("dto:DTO", "/templates/dto.java.ftl");
            customFile.put("dto:Criteria", "/templates/criteria.java.ftl");
            customFile.put("converter:Converter", "/templates/converter.java.ftl");
            builder.customFile(customFile);
        });

        generator.templateEngine(new EnhanceFreemarkerTemplateEngine());

        generator.execute();
    }
}
