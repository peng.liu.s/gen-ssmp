package ${package.ServiceImpl};

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${package.Parent}.converter.${entity}Converter;
import ${package.Entity}.${entity};
import ${package.Parent}.dto.${entity}Criteria;
import com.sptan.framework.core.ResultEntity;
import com.sptan.framework.mybatis.page.PageCriteria;
import ${package.Parent}.dto.${entity}DTO;
import ${package.Mapper}.${table.mapperName};
<#if table.serviceInterface>
import ${package.Service}.${table.serviceName};
</#if>
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * ${table.comment!} 服务实现类.
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
@Slf4j
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>()<#if table.serviceInterface>, ${table.serviceName}</#if> {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}><#if table.serviceInterface>
    implements ${table.serviceName}</#if> {

    /**
     * 保存.
     *
     * @param dto the dto
     * @return the result entity
     */
    @Override
    public ResultEntity<${entity}DTO> save(${entity}DTO dto) {
        ${entity} entity = ${entity}Converter.INSTANCE.toEntity(dto);
        this.saveOrUpdate(entity);
        return ResultEntity.ok(${entity}Converter.INSTANCE.toDto(entity));
    }

    /**
     * 删除.
     *
     * @param id the id
     * @return the result entity
     */
    @Override
    public ResultEntity<Boolean> delete(Long id) {
        ${entity} entity = getById(id);
        if (entity == null || Objects.equals(true, entity.getDeleteFlag())) {
            return ResultEntity.error("数据不存在");
        }
        entity.setDeleteFlag(true);
        this.saveOrUpdate(entity);
        return ResultEntity.ok(true);
    }

    /**
     * 查看详情.
     *
     * @param id the id
     * @return the result entity
     */
    @Override
    public ResultEntity<${entity}DTO> detail(Long id) {
        ${entity} entity = baseMapper.selectById(id);
        if (entity == null || Objects.equals(true, entity.getDeleteFlag())) {
            return ResultEntity.error("数据不存在");
        }
        ${entity}DTO dto = ${entity}Converter.INSTANCE.toDto(entity);
        return ResultEntity.ok(dto);
    }

    /**
     * 根据条件分页查询.
     *
     * @param criteria the criteria
     * @return the result entity
     */
    @Override
    public ResultEntity<IPage<${entity}DTO>> search(${entity}Criteria criteria) {
        LambdaQueryWrapper<${entity}> wrapper = getSearchWrapper(criteria);
        IPage<${entity}> entityPage = this.baseMapper.selectPage(PageCriteria.toPage(criteria), wrapper);
        return ResultEntity.ok(entityPage.convert(${entity}Converter.INSTANCE::toDto));
    }

    private LambdaQueryWrapper<${entity}> getSearchWrapper(${entity}Criteria criteria) {
        LambdaQueryWrapper<${entity}> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(${entity}::getDeleteFlag, false);
        if (criteria == null) {
            return wrapper;
        }
        return wrapper;
    }
}
</#if>
