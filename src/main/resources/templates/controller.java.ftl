package ${package.Controller};

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sptan.framework.core.ResultEntity;
import ${package.Parent}.dto.${entity}DTO;
import ${package.Parent}.dto.${entity}Criteria;
import ${package.Service}.${table.serviceName};
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@RestController
@RequestMapping("/biz<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")

<#if springdoc>
<#elseif swagger>
    @Api(tags = "${table.comment!}控制器")
</#if>
@RequiredArgsConstructor
@Slf4j
public class ${table.controllerName} {

    /**
     * The ${entity} service.
     */
    private final ${entity}Service ${table.serviceName?substring(0, 1)?lower_case}${table.serviceName?substring(1)};

    /**
     * 根据条件查询.
     *
     * @param criteria the criteria
     * @return the contract info
     */
    @PostMapping("/search")
    @Operation(summary = "条件查询", description = "根据条件分页查询")
    @Parameters({
        @Parameter(in = ParameterIn.HEADER, name = "Authorization", description = "标识用户信息的请求头", required = true)
    })
    public ResponseEntity<ResultEntity<IPage<${entity}DTO>>> search(@Validated @RequestBody ${entity}Criteria criteria) {
        ResultEntity<IPage<${entity}DTO>> resultEntity = ${table.serviceName?substring(0, 1)?lower_case}${table.serviceName?substring(1)}.search(criteria);
        return ResponseEntity.ok(resultEntity);
    }

    /**
     * 查看详情.
     *
     * @param id the id
     * @return the response entity
     */
    @PostMapping("/detail/{id}")
    @Operation(summary = "详情", description = "根据ID查看详情")
    @Parameters({
        @Parameter(in = ParameterIn.HEADER, name = "Authorization", description = "标识用户信息的请求头", required = true),
        @Parameter(in = ParameterIn.PATH, name = "id", description = "ID", required = true)
    })
    public ResponseEntity<ResultEntity<${entity}DTO>> detail(@PathVariable("id") Long id) {
        ResultEntity<${entity}DTO> resultEntity = ${table.serviceName?substring(0, 1)?lower_case}${table.serviceName?substring(1)}.detail(id);
        return ResponseEntity.ok(resultEntity);
    }

    /**
     * 删除.
     *
     * @param id the id
     * @return the response entity
     */
     @PostMapping("/delete/{id}")
     @Operation(summary = "删除", description = "根据ID逻辑删除对象")
     @Parameters({
          @Parameter(in = ParameterIn.HEADER, name = "Authorization", description = "标识用户信息的请求头", required = true),
          @Parameter(in = ParameterIn.PATH, name = "id", description = "ID", required = true)
     })
     public ResponseEntity<ResultEntity<Boolean>> delete(@PathVariable("id") Long id) {
         ResultEntity<Boolean> resultEntity = ${table.serviceName?substring(0, 1)?lower_case}${table.serviceName?substring(1)}.delete(id);
         return ResponseEntity.ok(resultEntity);
     }

     /**
      * 保存.
      *
      * @param dto the dto
      * @return the response entity
      */
     @PostMapping("/save")
     @Operation(summary = "保存", description = "保存对象,包括新增和修改")
     @Parameters({
         @Parameter(in = ParameterIn.HEADER, name = "Authorization", description = "标识用户信息的请求头", required = true)
     })
     public ResponseEntity<ResultEntity<${entity}DTO>> save(@Validated @RequestBody ${entity}DTO dto) {
         ResultEntity<${entity}DTO> resultEntity = ${table.serviceName?substring(0, 1)?lower_case}${table.serviceName?substring(1)}.save(dto);
         return ResponseEntity.ok(resultEntity);
     }

}
