package ${package.Parent}.converter;

import ${package.Entity}.${entity};
import ${package.Parent}.dto.${entity}DTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>
 * ${table.comment!} Mapstruct转换接口.
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Mapper(componentModel = "spring")
public interface ${entity}Converter {

    /**
     * The constant INSTANCE.
     */
    ${entity}Converter INSTANCE = Mappers.getMapper(${entity}Converter.class);


    /**
     * To dto base station dto.
     *
     * @param entity the entity
     * @return the base station dto
     */
    ${entity}DTO toDto(${entity} entity);

    /**
     * dto to entity.
     *
     * @param dto the entity
     * @return the base station brief dto
     */
    ${entity} toEntity(${entity}DTO dto);

    /**
     * To dto list.
     *
     * @param entities the entities
     * @return the list
     */
    List<${entity}DTO> toDtoList(List<${entity}> entities);

    /**
     * To entity list.
     *
     * @param dtos the dtos
     * @return the list
     */
     List<${entity}> toEntities(List<${entity}DTO> dtos);
}
