package com.sptan.gen;

import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 代码生成器支持自定义[DTO\VO等]模版
 *
 * @author: austin
 * @since: 2023/2/9 13:00
 */
@Component
public class EnhanceFreemarkerTemplateEngine extends FreemarkerTemplateEngine {

    @Override
    protected void outputCustomFile(List<CustomFile> customFiles, TableInfo tableInfo, Map<String, Object> objectMap) {
        String entityName = tableInfo.getEntityName();
        String parentPath = this.getPathInfo(OutputFile.parent);
        customFiles.forEach((file) -> {
            String filePath = StringUtils.hasText(file.getFilePath()) ? file.getFilePath() : parentPath;
            if (StringUtils.hasText(file.getPackageName())) {
                filePath = filePath + File.separator + file.getPackageName();
                filePath = filePath.replaceAll("\\.", "\\" + File.separator);
            }

            // 自定义的包名:Entity 后缀：model/qo:GetQO
            String[] customFileNames = file.getFileName().split(":");
            String fileName = filePath + (!customFileNames[0].startsWith("/") ? File.separator : "")
                + customFileNames[0] + File.separator + entityName + customFileNames[1] + ".java";
            String templatePath = file.getTemplatePath();
            if (!templatePath.endsWith(".ftl")) {
                templatePath += ".ftl";
            }

            this.outputFile(new File(fileName), objectMap, templatePath, file.isFileOverride());
        });
    }
}
